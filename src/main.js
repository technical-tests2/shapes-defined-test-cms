import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import VueAxios from 'vue-axios';
import Notifications from '@kyvg/vue3-notification';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
require('@/store/subscriber');
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 401 && store.getters['auth/authenticated']) {
        store.dispatch("auth/handleUnAuthenticatedResponse").then(() => {
            router.push('/login');
        });
    }
    return Promise.reject(error);
});

store.dispatch("auth/attempt", localStorage.getItem('token')).then(() => {
    createApp(App).use(store).use(router).use(Notifications).use(VueAxios,axios).use(ElementPlus).mount('#app')
});
