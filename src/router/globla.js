import HomeView from "@/views/HomeView";

export let homeRoute = {
    path: '/home',
    name: 'home',
    component: HomeView
};


