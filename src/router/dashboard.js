import DashboardView from "@/views/dashboard/DashboardView";

export let dashboardRoute = {
    path: '/dashboard',
    name: 'dashboard',
    component: DashboardView
};


