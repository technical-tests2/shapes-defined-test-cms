import {createRouter, createWebHistory} from 'vue-router'

import store from '@/store';
import routes from './routes';


const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

let unauthenticatedNames = ['login', 'not-found'];
router.beforeEach((to, from, next) => {
    if (!unauthenticatedNames.includes(to.name) && !store.getters['auth/authenticated']) {
        return next({
            name: 'login'
        });
    }
    return next();
})
export default router
