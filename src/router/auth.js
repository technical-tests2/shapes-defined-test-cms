import LoginView from "@/views/auth/LoginView";
import ProfileView from "@/views/auth/ProfileView";

export let loginRoute = {
    path: '/login',
    name: 'login',
    component: LoginView
};

export let profileRoute = {
    path: '/profile',
    name: 'profile',
    component: ProfileView
};


