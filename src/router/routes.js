import {homeRoute} from '@/router/globla';
import {dashboardRoute} from '@/router/dashboard';
import {loginRoute,profileRoute} from "@/router/auth";
import NotFound from '@/views/errors/NotFoundView';
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  loginRoute,
  homeRoute,
  dashboardRoute,
  profileRoute,
  { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
]

export default routes
